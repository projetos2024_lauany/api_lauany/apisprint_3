// app.js
const express = require('express');
const cors = require('cors');
const apiRoutes = require('./route/route');
const app = express();

app.use(express.json());
app.use(cors());
app.use('/api', apiRoutes);

app.listen(5000, () => {
    console.log('Quadras Conectadas');
});

