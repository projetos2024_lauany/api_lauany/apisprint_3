const express = require('express');
const cors = require('cors');
const apiRoutes = require('./route/route');

class AppController {
    constructor() {
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.express.use(express.json());
        this.express.use(cors());
    }

    routes() {
        this.express.use('/api/', apiRoutes);
        this.express.get("/teste/", (_, res) => {
            res.send({ status: " Testando a Api " });
        });
    }
}

module.exports = new AppController().express;
