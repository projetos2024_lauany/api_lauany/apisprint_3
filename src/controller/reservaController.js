const connect = require("../db/connect");

class ReservaController {
    static async postReserva(req, res) {
        const { ReservaID, QuadraID, TipoQuadra, UsuarioID, Data, HoraInicial, HoraFinal, ValorPorHora } = req.body;
        
        if (!ReservaID || !QuadraID || !TipoQuadra || !UsuarioID || !Data || !HoraInicial || !HoraFinal || !ValorPorHora) {
            return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
        }

        if (new Date(HoraInicial) >= new Date(HoraFinal)) {
            return res.status(400).json({ error: "A hora de início deve ser anterior à hora de término" });
        }
    

        const query = `SELECT * FROM reservas WHERE QuadraID = ? AND Data = ? AND ((HoraInicial <= ? AND HoraFinal >= ?) OR (HoraInicial >= ? AND HoraFinal <= ?) OR (HoraInicial <= ? AND HoraFinal >= ?))`;
        connect.query(query, [QuadraID, Data, HoraInicial, HoraFinal, HoraInicial, HoraFinal, HoraInicial, HoraFinal], function (err, result) {
            if (err) {
                console.error("Erro ao verificar reservas existentes:", err);
                return res.status(500).json({ error: "Erro interno do servidor" });
            }
            if (result.length > 0) {
                return res.status(400).json({ error: "Já existe uma reserva para esta quadra nesta hora e neste dia" });
            }

            const queryInsert = `INSERT INTO reservas (ReservaID, QuadraID, TipoQuadra, UsuarioID, Data, HoraInicial, HoraFinal, ValorPorHora) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`;
            connect.query(queryInsert, [ReservaID, QuadraID, TipoQuadra, UsuarioID, Data, HoraInicial, HoraFinal, ValorPorHora], function (err) {
                if (err) {
                    console.log(err);
                    res.status(500).json({ error: "Erro ao cadastrar reserva" });
                    return;
                }
                console.log("Reserva cadastrada com sucesso");
                res.status(201).json({ message: "Reserva cadastrada com sucesso" });
            });
        });
    }

    static async getAllReserva(req, res) {
        try {
            const query = "SELECT * FROM reservas";
            connect.query(query, function (err, result) {
                if (err) {
                    console.error("Erro ao obter reservas:", err);
                    return res.status(500).json({ error: "Erro interno do servidor" });
                }
                console.log("Reservas obtidas com sucesso");
                res.status(200).json({ message: "Obtendo todas as reservas", reservas: result });
            });
        } catch (error) {
            console.error("Erro ao executar a consulta:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }

    static async getAllReservaByID(req, res) {
        const ReservaID = req.params.id;

        try {
            const query = `SELECT * FROM reservas WHERE ReservaID = '${ReservaID}'`;
            connect.query(query, function (err, result) {
                if (err) {
                    console.error("Erro ao obter reserva:", err);
                    return res.status(500).json({ error: "Erro interno do servidor" });
                }

                if (result.length === 0) {
                    return res.status(404).json({ error: "Reserva não encontrada" });
                }

                console.log("Reserva obtida com sucesso");
                res.status(200).json({ message: "Obtendo a reserva com ID: " + ReservaID, reserva: result[0] });
            });
        } catch (error) {
            console.error("Erro ao executar a consulta:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }

    static async updateReserva(req, res) {
        const ReservaID = req.params.id;
        const { QuadraID, TipoQuadra, UsuarioID, Data, HoraInicial, HoraFinal, ValorPorHora } = req.body;

        if (!QuadraID || !TipoQuadra || !UsuarioID || !Data || !HoraInicial || !HoraFinal || !ValorPorHora) {
            return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
        }

        if (new Date(HoraInicial) >= new Date(HoraFinal)) {
            return res.status(400).json({ error: "A hora de início deve ser anterior à hora de término" });
        }

        const query = `SELECT * FROM reservas WHERE QuadraID = ? AND Data = ? AND ((HoraInicial <= ? AND HoraFinal >= ?) OR (HoraInicial >= ? AND HoraFinal <= ?) OR (HoraInicial <= ? AND HoraFinal >= ?))`;
        connect.query(query, [QuadraID, Data, HoraInicial, HoraFinal, HoraInicial, HoraFinal, HoraInicial, HoraFinal], function (err, result) {
            if (err) {
                console.error("Erro ao verificar reservas existentes:", err);
                return res.status(500).json({ error: "Erro interno do servidor" });
            }
            if (result.length > 0) {
                return res.status(400).json({ error: "Já existe uma reserva para esta quadra nesta hora e neste dia" });
            }

            const queryUpdate = `UPDATE reservas SET QuadraID = ?, TipoQuadra = ?, UsuarioID = ?, Data = ?, HoraInicial = ?, HoraFinal = ?, ValorPorHora = ? WHERE ReservaID = ?`;
            connect.query(queryUpdate, [QuadraID, TipoQuadra, UsuarioID, Data, HoraInicial, HoraFinal, ValorPorHora, ReservaID], function (err) {
                if (err) {
                    console.error("Erro ao atualizar reserva:", err);
                    res.status(500).json({ error: "Erro interno do servidor" });
                    return;
                }
                console.log("Reserva atualizada com sucesso");
                res.status(200).json({ message: "Reserva atualizada com sucesso" });
            });
        });
    }

    static async deleteReserva(req, res) {
        const ReservaID = req.params.id;

        const query = `DELETE FROM reservas WHERE ReservaID = ?`;

        connect.query(query, [ReservaID], function (err) {
            if (err) {
                console.error("Erro ao deletar reserva:", err);
                res.status(500).json({ error: "Erro interno do servidor" });
                return;
            }
            console.log("Reserva deletada com sucesso");
            res.status(200).json({ message: "Reserva deletada com sucesso" });
        });
    }
}

module.exports = ReservaController;